# README #

## No Need to Install Python or other dependencies. Simply Run Analyzer from the terminal/cmd.Supports 64bit Linux OS and 64bit Windows OS##

### *analyzer - Linux OS* 

### *analyzer.exe - Windows OS* ###

Download Result PDF files using [downloader](https://bitbucket.org/shaiju1983/cu_pdf_downloader).

**Then download sm_resultAnalysis from [Downloads](https://bitbucket.org/shaiju1983/sm-result-analysis/downloads) of Bitbucket.**

**Next Run SM Result Analysis as shown below:**

*Linux*
```
./analyzer path_to_folder_having_pdfs
```
*Windows*
```
analyzer.exe path_to_folder_having_pdfs
```

**If successful, an excel file named "results.xls" will be generated.**